import argparse
import os.path

import pandas as pd


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file_name", metavar="F", type=str, help="gzipped file name")
    args = parser.parse_args()
    
    file_name = args.file_name

    target_file_name = "top-"+file_name.replace(".gz", ".csv")

    if os.path.exists(target_file_name):
        print(target_file_name + "already exists... exiting")
        exit()

    page_views = pd.read_csv(file_name, sep=" ", header=0, names=["domain_code","page_title","count_views","total_response_size"])
    
    block_list = pd.read_csv("./blacklist_domains_and_pages", sep=" ", header=0, names=["domain_code", "page_title"])
    
    cleaned_page_views = pd.merge(page_views, block_list, indicator=True, how="outer").query('_merge=="left_only"').drop("_merge", axis=1)

    summed_page_views = cleaned_page_views.groupby(["domain_code", "page_title"])[["count_views"]].sum().reset_index()

    top_twenty_five = summed_page_views.groupby(["domain_code"]).apply(lambda x: x.nlargest(25, columns=["count_views"]))

    top_twenty_five.to_csv(target_file_name, sep=" ", columns=["domain_code", "page_title", "count_views"], index=False)