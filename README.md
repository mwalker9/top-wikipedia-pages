# Top Wikipedia Pages

Computes the top 25 pages on Wikipedia for each of the Wikipedia sub-domains

# Instructions for Running

## Requirements
bash with CURL
Python3 with Pandas
Connectivity to the internet

## How to run
If you have a shell with curl installed as well as Python3 with Pandas (v1.1.5) installed, all you have to do to run this is execute the `do_run.sh` script.  I do this by running `bash do_run.sh` on my terminal.  Then there are a few parameters you can pass, having to do with the timeframes to process.

The format accepted for timestamps as paramters is `yyyy-mm-ddTHH:MM:SS` even though only the day and hour part are actually used.

The `-s` flag specifies the start timestamp of the timeframe to pull and process the data for.  If the `-s` flag is not passed, it defaults to 24 hours ago.  

The `-e` flag specifies the end timestamp of the timeframe to pull and process the data for.  If the `-e` flag is not passed, it defaults to only pulling a single hour starting with the start timestamp.

# Outputs

Once the program is ran for a specific hour, it produces two separate files.  The first file is the downloaded data from Wikipedia, in the format `pageviews-yyyymmdd-HH0000.gz`.  The second file is the results file which has the same name as the data from Wikipedia, just with a `top-` prefix added to the file name.

# What I would want for production operation

To run this in a production setting, I would want better workflow coordination.  For example, in Airflow, I could make the file download separate from the data processing part so that way if one of the parts had an error, it could be reran atomically.

I would also want to version control more of the environment around this, for this to be used in a production application.  For this, I have just said "use Ubuntu", "have Curl and Pandas".  For an actual process I would want to run in production, I would use Docker to make sure that the code is running in the same version-controlled environment for local development, testing, and then production.  This would also include using a requirements file to version control the Python dependencies.

For a production application, I would also want to make sure that CURL threw errors if it couldn't find the file from Wikipedia.  I think right now it is just printing out the 404 message into a file if it can't find a data file which breaks the downstream task.

# What might change if this needed to be ran every hour automatically

Using something like Airflow makes it very easy to run this every hour automatically.  I would just have Airflow pass the execution date as the timestamp to the process and schedule the DAG to run every hour.  If we know that the DAG should run for every hour though, I would recommend adding some additional monitoring logic to the process to make sure that every file we expect to have has actually been downloaded and processed, just in case something has gone wrong and we have missed an hour somehow.  This monitoring could be an additional Airflow task at the end of the process that examines all of the timestamps on the files and makes sure there are no gaps.

# How I would test this

I would test this by making it more atomic as I mentioned before.  I would keep the file downloading as its own task and then the data processing could be easily unit tested by creating some test datasets and expected results files.

We could also have some in-process data testing to make sure the data is as expected before processing it.  For example, before the Python process runs to calculate the data, there could be a quick sanity check to make sure the Wikipedia source data file has been downloaded and that it is actually in some sort of CSV format.

# How would I improve the design

For a production application, I probably wouldn't use Pandas.  I used Pandas because it's something familiar to me, and easy to reason about and setup quickly on my laptop.  I think data processing like this is better suited for SQL or more multiprocessing approaches such as Spark.

This design also has an inefficiency because it is loading the blocklist into memory for every single hour that needs to be processed.  If I were to spend more time on this, I would try to come up with a way to only load the blocklist once even if I was processing more than one hour of data.