#!/bin/bash

while getopts s:e: flag
do
    case "${flag}" in
        s) start_date=${OPTARG};;
        e) end_date=${OPTARG};;
    esac
done

if [ -z $start_date ];
then
    right_now=$(date -u "+%s")
    from_dt=$((right_now - 3600*24))
else
    from_dt=$(date -u -d "$start_date"  "+%s")
fi

if [ -z $end_date ]; 
then
    to_dt=$((from_dt + 3600))
else
    to_dt=$(date -u -d "$end_date" "+%s")
fi


#https://dumps.wikimedia.org/other/pageviews/2021/2021-05/pageviews-20210501-070000.gz
while [ "$from_dt" -lt "$to_dt" ]; do

    year=$(date -d"@$from_dt" -u "+%Y")
    month=$(date -d"@$from_dt" -u "+%m")
    day=$(date -d"@$from_dt" -u "+%d")
    hour=$(date -d"@$from_dt" -u "+%H")

    echo "processing $year, $month, $day, $hour...."

    FILE="pageviews-$year$month$day-${hour}0000.gz"
    
    echo $FILE

    if test -f "$FILE"; 
    then
        echo "$FILE exists... not reprocessing"
    else
        curl "https://dumps.wikimedia.org/other/pageviews/$year/$year-$month/$FILE" --output $FILE
        python3 process_hour_file.py $FILE
    fi
    from_dt=$((from_dt + 3600))
done
